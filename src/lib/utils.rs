use std::{
    fs,
    io::{self, Write},
};

use rug::Rational;

const SAVEFILE_RATIONAL: &str = "./pi.txt";
const SAVEFILE_DECIMAL: &str = "./pi-decimal.txt";

pub fn save(pi: &Rational) -> io::Result<()> {
    let mut file_rational = fs::OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(SAVEFILE_RATIONAL)?;
    file_rational.write(pi.to_string().as_bytes())?;
    let mut file_decimal = fs::OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(SAVEFILE_DECIMAL)?;
    file_decimal.write(pi.to_f64().to_string().as_bytes())?;
    Ok(())
}
