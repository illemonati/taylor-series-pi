use rug::Rational;
use taylor_series_pi::utils::save;

const END: usize = 300001;

fn main() {
    let mut pi_over_4 = Rational::from(1);
    for n in 1..=END {
        let numerator = if 1 == n & 1 { -1 } else { 1 };
        let term = Rational::from((numerator, 2 * n + 1));
        pi_over_4 += term;
        if n % 10001 == 0 {
            println!("term: {}", n);
            let pi = Rational::from(&pi_over_4 * 4);
            println!("pi: {}", pi.to_f64());
            save(&pi).expect("Error Saving Pi")
        }
    }
    println!("term: {}", END);
    let pi = Rational::from(&pi_over_4 * 4);
    println!("pi: {}", pi.to_f64());
    save(&Rational::from(&pi * 4)).expect("Error Saving Pi")
}
