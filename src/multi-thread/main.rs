use rug::{Integer, Rational};

use rayon::prelude::*;

use taylor_series_pi::utils::save;

const CHUNK_AMOUNT: u32 = 100000;
const NUM_CHUNKS_PER_EPOCH: u32 = 16;

const END: u32 = NUM_CHUNKS_PER_EPOCH * CHUNK_AMOUNT * 50;

fn main() {
    let mut pi_over_4 = Rational::from(1);
    let mut n = Integer::from(1);
    let total_epoch = END / NUM_CHUNKS_PER_EPOCH / CHUNK_AMOUNT;
    for _epoch in 0..total_epoch {
        let epoch_sum: Rational = (0..NUM_CHUNKS_PER_EPOCH)
            .into_par_iter()
            .map(|i| {
                let chunk_start = Integer::from(&n + i * CHUNK_AMOUNT);
                calculate_chunk_term(&chunk_start, CHUNK_AMOUNT)
            })
            .sum();
        pi_over_4 += epoch_sum;
        n += NUM_CHUNKS_PER_EPOCH * CHUNK_AMOUNT;
        println!("term: {}", n);
        let pi = Rational::from(&pi_over_4 * 4);
        println!("pi: {}", &pi.to_f64());
        save(&pi).expect("Error Saving Pi");
    }
}

fn calculate_chunk_term(start: &Integer, amount: u32) -> Rational {
    let mut chunk_term = Rational::from(0);
    let mut n = Integer::from(start);
    for _ in 0..amount {
        let n_1 = Integer::from(&n);
        let numerator = if n.is_odd() { -1 } else { 1 };
        let term = Rational::from((numerator, 2 * Integer::from(n_1) + 1));
        chunk_term += term;
        n = n + 1;
    }
    chunk_term
}
